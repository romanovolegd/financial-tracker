package sirius.tinkoff.demo.myspringboot.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import sirius.tinkoff.demo.myspringboot.converter.WalletDtoToWalletConverter;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;
import sirius.tinkoff.demo.myspringboot.data.repository.CurrencyRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.WalletRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

class WalletServiceTest {
    @Mock
    private WalletRepository walletRepository;
    @Mock
    private WalletDtoToWalletConverter walletDtoToWalletConverter;
    @Mock
    private PersonRepository personRepository;
    @Mock
    private CurrencyService currencyService;
    @Mock
    private CurrencyRepository currencyRepository;
    @Mock
    private TransactionService transactionService;


    WalletService walletService = createWalletService();

    WalletService createWalletService() {
        return new WalletService(walletRepository
                ,walletDtoToWalletConverter
                ,personRepository
                ,currencyService
                ,currencyRepository
                ,transactionService);
    }



    @Test
    void getAll() {
    }

    @Test
    void getWalletScreen() {
    }

    @Test
    void create() {
    }
    @Test
    void countIncome(){

        List<Transaction> transactionList = new ArrayList<>();

        Transaction transaction = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category = new Category().setValue(0);
        transaction.setCategory(category);

        Transaction transaction1 = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category1 = new Category().setValue(1);
        transaction1.setCategory(category1);

        Transaction transaction2 = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category2 = new Category().setValue(0);
        transaction2.setCategory(category2);

        transactionList.add(transaction);
        transactionList.add(transaction1);
        transactionList.add(transaction2);

        System.out.println(transactionList);

        Wallet wallet = new Wallet().setTransaction(transactionList);

        BigDecimal actual = BigDecimal.valueOf(200);
        BigDecimal expected = walletService.countIncome(wallet);

        Assertions.assertEquals(actual, expected);

    }
    @Test
    void countConsumption(){

        List<Transaction> transactionList = new ArrayList<>();

        Transaction transaction = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category = new Category().setValue(0);
        transaction.setCategory(category);

        Transaction transaction1 = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category1 = new Category().setValue(1);
        transaction1.setCategory(category1);

        Transaction transaction2 = new Transaction().setValue(BigDecimal.valueOf(100));
        Category category2 = new Category().setValue(0);
        transaction2.setCategory(category2);

        transactionList.add(transaction);
        transactionList.add(transaction1);
        transactionList.add(transaction2);

        System.out.println(transactionList);

        Wallet wallet = new Wallet().setTransaction(transactionList);

        BigDecimal actual = BigDecimal.valueOf(100);
        BigDecimal expected = walletService.countConsumption(wallet);

        Assertions.assertEquals(actual, expected);

    }
}