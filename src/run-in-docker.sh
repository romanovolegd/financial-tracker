#!/bin/bash

docker pull alexyakovlev90/sirius-app:latest
docker rm $(docker inspect --format="{{.Id}}" sirius-app)
docker run -d -p 9090:9090 \
    --env DB_HOST=pg_processor --env DB_NAME=root --env DB_USERNAME=root --env DB_PASSWORD=password \
    --name=sirius-app \
    alexyakovlev90/sirius-app:latest
docker ps
