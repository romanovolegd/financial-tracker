package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Сущность транзакции
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Категория")
@ToString
public class TransactionCategoryDto {
    private Long transactionId;
    @Schema(description = "Сумма транзакции")
    private String value;
    @Schema(description = "Тип категории (доход/расход)")
    private int category;
    @Schema(description = "Какая из категорий (супермаркет/зарплата)")
    private String categoryName;
    @Schema(description = "Иконка")
    private int icon;
    @Schema(description = "Цвет иконки")
    private int colour;
    @Schema(description = "Валюта")
    private String currency;
    @Schema(description = "Время")
    private Long time;
    @Schema(description = "Кошелек id")
    private Long walletId;
    @Schema(description = "Какая из категорий (супермаркет/зарплата) id")
    private Long categoryId;

}
