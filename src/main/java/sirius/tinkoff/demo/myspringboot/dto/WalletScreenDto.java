package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Кошелёк")
@ToString
public class WalletScreenDto {
    String balance;
    String consumption;
    String income;
    CurrencyDto currencyDto;
    List<WalletDto> wallets;
}
