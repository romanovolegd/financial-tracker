package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


/**
 * Сущность кошелька
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Кошелёк")
@ToString

public class WalletDto {
    private Long walletId;
    @Schema(description = "id пользователя, к которому привязан кошелёк")
    private Long personId;
    @Schema(description = "Валюта кошелька")
    private String currency;
    @Schema(description = "Название кошелька", required = true)
    private String name;
    @Schema(description = "Лимит кошелька")
    private String limit;
    @Schema(description = "Общее количество денег")
    private String amountMoney;
    @Schema(description = "Количество доходов")
    private String income;
    @Schema(description = "Количество расходов")
    private String consumption;
    @Schema(description = "Закрыт или открыт кошелек")
    private Boolean isHide;
    @Schema(description = "Превышен ли лимит не кошельке")
    private Boolean isExceeded;
}

