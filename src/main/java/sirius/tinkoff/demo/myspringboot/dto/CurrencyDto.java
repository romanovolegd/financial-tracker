package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Валюта")
@ToString
public class CurrencyDto {
    @Schema(description = "Имя валюты")
    private String firstCurrency;
    @Schema(description = "Курс")
    private String firstCourse;
    @Schema(description = "Поднялось ли по сравнению с предыдущим состоянием")
    private Boolean firstIsUp;

    private String secondCurrency;
    private String secondCourse;
    private Boolean secondIsUp;

    private String thirdCurrency;
    private String thirdCourse;
    private Boolean thirdIsUp;

}
