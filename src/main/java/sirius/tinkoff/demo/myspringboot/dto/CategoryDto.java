package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
/**
 * Сущность категории
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Категория")
@ToString
public class CategoryDto {
    private Long categoryId;
    @Schema(description = "Имя категории")
    private String name;
    @Schema(description = "Тип категории (доход/расход)")
    private Integer value;
    @Schema(description = "Иконка")
    private Integer icon;
    @Schema(description = "Цвет")
    private Integer colour;
    @Schema(description = "Id пользователя", required = true)
    private Long personId;
}