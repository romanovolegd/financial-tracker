package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Сущность категории
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Категория")
@ToString
public class PersonDto {
    private Long personId;
    @Schema(description = "Почта")
    private String mail;
}