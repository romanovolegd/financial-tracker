package sirius.tinkoff.demo.myspringboot.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


/**
 * Сущность транзакции
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Категория")
@ToString
public class TransactionDto {
    private Long transactionId;
    @Schema(description = "Сумма транзакции")
    private String value;
    @Schema(description = "Какая из категорий (супермаркет/зарплата) id")
    private Long categoryId;
    @Schema(description = "Валюта")
    private String currency;
    @Schema(description = "Время")
    private Long time;
    @Schema(description = "Кошелек id")
    private Long walletId;
}
