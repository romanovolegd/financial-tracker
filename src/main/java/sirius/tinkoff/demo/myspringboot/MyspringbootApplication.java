package sirius.tinkoff.demo.myspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import sirius.tinkoff.demo.myspringboot.service.api.CurrencyApiInterface;
import sirius.tinkoff.demo.myspringboot.service.api.CurrencyWeb;


@SpringBootApplication
@EnableFeignClients
public class MyspringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyspringbootApplication.class, args);
    }
}
