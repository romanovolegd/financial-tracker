package sirius.tinkoff.demo.myspringboot.data.entity;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
@Accessors(chain = true)
public class Currency {
    @Id
    @GeneratedValue(generator = "currency_seq")
    @Column(name = "currency_id")
    private Long currencyId;
    private String name;
    //3 english letters
    private String title;
    private BigDecimal course;
    @Column(name = "is_up")
    private Boolean isUp;


    public Currency(Long currencyId, String name, String title, BigDecimal course, Boolean isUp){
        this.currencyId=currencyId;
        this.name=name;
        this.title=title;
        this.course=course;
        this.isUp=isUp;
    }
}
