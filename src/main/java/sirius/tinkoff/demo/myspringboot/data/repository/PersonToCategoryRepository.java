package sirius.tinkoff.demo.myspringboot.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.demo.myspringboot.data.entity.PersonToCategory;

import java.util.List;

@Repository
public interface PersonToCategoryRepository extends JpaRepository<PersonToCategory,Long> {
    List<PersonToCategory> getAllByPersonPersonId(Long personId);
}
