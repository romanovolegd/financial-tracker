package sirius.tinkoff.demo.myspringboot.data.entity;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
@Accessors(chain = true)
public class Category {

    @Id
    @GeneratedValue(generator = "category_seq")
    @Column(name = "category_id")
    private Long categoryId;
    private String name;
    private Integer value;
    private Integer icon;
    private Integer colour;

    @OneToMany(cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "category_id")
    private List<PersonToCategory> personToCategory;

    @OneToMany
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "category_id")
    private List<Transaction> transaction;

    public Category(String name, Integer value, Integer icon, Integer colour) {
        this.value = value;
        this.name = name;
        this.icon = icon;
        this.colour = colour;
    }


}