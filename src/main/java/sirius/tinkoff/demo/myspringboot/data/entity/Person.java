package sirius.tinkoff.demo.myspringboot.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "application_user_seq", sequenceName = "application_user__seq")
@Accessors(chain = true)
public class Person {

    @Id
    @GeneratedValue(generator = "person_seq")
    @Column(name = "person_id")
    private Long personId;
    @Column(unique = true)
    private String mail;

    @JoinColumn(name = "person_id")
    @OneToMany(cascade = CascadeType.ALL)
    private List<Wallet> wallet;

    @JoinColumn(name = "person_id")
    @OneToMany(cascade = CascadeType.ALL)
    private List<PersonToCategory> personToCategory;

    public Person(String mail, List<Wallet> wallet) {
        this.mail=mail;
        this.wallet=wallet;
    }
}