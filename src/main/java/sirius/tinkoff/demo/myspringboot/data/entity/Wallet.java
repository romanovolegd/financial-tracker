package sirius.tinkoff.demo.myspringboot.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
@ToString(exclude = "name")
@NoArgsConstructor
@Transactional
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
@Accessors(chain = true)
public class Wallet {

    @Id
    @GeneratedValue(generator = "wallet_seq")
    @Column(name = "wallet_id")
    private Long walletId;
    private String name;
    @JoinColumn(name = "currency_id")
    @ManyToOne
    private Currency currency;
    @Column(name = "wallet_limit")
    private BigDecimal limit;
    @Column(name = "is_hide")
    private Boolean isHide;

    @JoinColumn(name = "person_id")
    @ManyToOne
    private Person person;

    @JoinColumn(name = "wallet_id")
    @OneToMany(cascade = CascadeType.ALL)
    private List<Transaction> transaction;

    public Wallet(String name, Currency currency, BigDecimal limit, Person person, List<Transaction> transaction, Boolean isHide) {
        this.name = name;
        this.currency = currency;
        this.limit = limit;
        this.person = person;
        this.transaction=transaction;
        this.isHide=isHide;
    }


}