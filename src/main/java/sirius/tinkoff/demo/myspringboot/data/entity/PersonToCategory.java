package sirius.tinkoff.demo.myspringboot.data.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "person_to_category_seq", sequenceName = "person_to_category_seq")
@Accessors(chain = true)
@ToString
@Table(name = "person_to_category")
public class PersonToCategory {

    @Id
    @GeneratedValue(generator = "person_to_category_seq")
    @Column(name = "person_to_category_id")
    private Long personToCategoryId;

    @JoinColumn(name = "person_id")
    @ManyToOne
    private Person person;

    @JoinColumn(name = "category_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private Category category;

}