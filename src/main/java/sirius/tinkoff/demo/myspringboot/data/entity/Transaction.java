package sirius.tinkoff.demo.myspringboot.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "transaction_seq", sequenceName = "transaction_seq")
@Accessors(chain = true)
public class Transaction {

    @Id
    @GeneratedValue(generator = "transaction_seq")
    @Column(name = "transaction_id")
    private Long transactionId;

    private BigDecimal value;
    private Long time;

    @JoinColumn(name = "currency_id")
    @ManyToOne
    private Currency currency;

    @JoinColumn(name = "wallet_id")
    @ManyToOne
    private Wallet wallet;

    @JoinColumn(name = "category_id")
    @ManyToOne
    private Category category;

    public Transaction(BigDecimal value, Currency currency, Long time, Wallet wallet, Category category) {
        this.value = value;
        this.currency = currency;
        this.time = time;
        this.wallet = wallet;
        this.category = category;
    }


}

