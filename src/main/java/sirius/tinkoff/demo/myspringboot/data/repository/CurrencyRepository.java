package sirius.tinkoff.demo.myspringboot.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.demo.myspringboot.data.entity.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Long> {
    Currency getByTitle(String title);
}
