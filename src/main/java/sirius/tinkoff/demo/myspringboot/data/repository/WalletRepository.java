package sirius.tinkoff.demo.myspringboot.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;

@Repository
public interface WalletRepository extends JpaRepository<Wallet,Long> {
}