package sirius.tinkoff.demo.myspringboot.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;


@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {
}
