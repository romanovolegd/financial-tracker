package sirius.tinkoff.demo.myspringboot.scheduled;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.service.CurrencyService;

@Component
@RequiredArgsConstructor
public class currencyScheduled {

    private final CurrencyService currencyService;

    @Scheduled(cron = "0 0 * ? * *")
    public void getCurrency(){
        currencyService.getCurrencyAndUpdate();
        System.out.println("updated currencies");
    }
}

