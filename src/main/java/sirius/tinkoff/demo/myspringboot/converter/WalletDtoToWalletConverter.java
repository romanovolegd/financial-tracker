package sirius.tinkoff.demo.myspringboot.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.data.entity.Person;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;
import sirius.tinkoff.demo.myspringboot.data.repository.CurrencyRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonRepository;
import sirius.tinkoff.demo.myspringboot.dto.WalletDto;

import java.math.BigDecimal;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WalletDtoToWalletConverter {

    private final PersonRepository personRepository;
    private final CurrencyRepository currencyRepository;

    public Wallet convert(WalletDto dto) {

        Long personId = dto.getPersonId();
        Optional<Person> personOptional = personRepository.findById(personId);

        if (personOptional.isPresent()){
            Person person = personOptional.get();
            String limit=dto.getLimit();
            if (limit == null){limit="0";}
            return new Wallet()
                    .setWalletId(dto.getWalletId())
                    .setName(dto.getName())
                    .setLimit(new BigDecimal(limit))
                    .setPerson(person)
                    .setCurrency(currencyRepository.getByTitle(dto.getCurrency()))
                    .setIsHide(dto.getIsHide());
        }
        else {return null;}
    }

    public WalletDto convert(Wallet wallet) {
        String limit=wallet.getLimit().toString();
        if (limit.equals("0")){limit=null;}
        return new WalletDto()
                .setWalletId(wallet.getWalletId())
                .setName(wallet.getName())
                .setLimit(limit)
                .setPersonId(wallet.getPerson().getPersonId())
                .setIsHide(wallet.getIsHide())
                .setCurrency(wallet.getCurrency().getTitle());

    }

}
