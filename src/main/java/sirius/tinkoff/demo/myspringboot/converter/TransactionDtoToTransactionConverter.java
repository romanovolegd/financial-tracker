package sirius.tinkoff.demo.myspringboot.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;
import sirius.tinkoff.demo.myspringboot.data.repository.CategoryRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.CurrencyRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.WalletRepository;
import sirius.tinkoff.demo.myspringboot.dto.TransactionDto;
import sirius.tinkoff.demo.myspringboot.dto.WalletDto;

import java.math.BigDecimal;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TransactionDtoToTransactionConverter {

    private final WalletRepository walletRepository;
    private final CategoryRepository categoryRepository;
    private final CurrencyRepository currencyRepository;

    public Transaction convert(TransactionDto dto) {

        Long walletId = dto.getWalletId();
        Long categoryId = dto.getCategoryId();

        Optional<Wallet> walletOptional = walletRepository.findById(walletId);
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

        if (walletOptional.isPresent() && categoryOptional.isPresent()) {
            Wallet wallet = walletOptional.get();
            Category category = categoryOptional.get();
            return new Transaction()
                    .setValue(new BigDecimal(dto.getValue()))
                    .setTime(dto.getTime())
                    .setCurrency(currencyRepository.getByTitle(dto.getCurrency()))
                    .setWallet(wallet)
                    .setCategory(category);

        } else {
            return null;
        }
    }

    public TransactionDto convert(Transaction transaction) {
        return new TransactionDto()
                .setTransactionId(transaction.getTransactionId())
                .setValue(transaction.getValue().toString())
                .setTime(transaction.getTime())
                .setCurrency(transaction.getCurrency().getTitle())
                .setWalletId(transaction.getWallet().getWalletId())
                .setCategoryId(transaction.getCategory().getCategoryId());

    }
}
