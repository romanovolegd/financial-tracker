package sirius.tinkoff.demo.myspringboot.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;
import sirius.tinkoff.demo.myspringboot.dto.TransactionCategoryDto;

@Component
@RequiredArgsConstructor
public class TransactionCategoryDtoToTransactionCategoryConverter {
    public TransactionCategoryDto convert(Transaction transaction, Category category) {
        return new TransactionCategoryDto()
                .setTransactionId(transaction.getTransactionId())
                .setValue(transaction.getValue().toString())
                .setTime(transaction.getTime())
                .setCurrency(transaction.getCurrency().getTitle())
                .setWalletId(transaction.getWallet().getWalletId())
                .setCategoryName(category.getName())
                .setColour(category.getColour())
                .setIcon(category.getIcon())
                .setCategory(category.getValue())
                .setCategoryId(category.getCategoryId());
    }
}
