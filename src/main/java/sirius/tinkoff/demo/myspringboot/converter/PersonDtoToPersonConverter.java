package sirius.tinkoff.demo.myspringboot.converter;

import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.data.entity.Person;
import sirius.tinkoff.demo.myspringboot.dto.PersonDto;

@Component
public class PersonDtoToPersonConverter {
    public Person convert(PersonDto dto) {
        return new Person()
                .setPersonId(dto.getPersonId())
                .setMail(dto.getMail());
    }

    public PersonDto convert(Person person) {
        return new PersonDto()
                .setPersonId(person.getPersonId())
                .setMail(person.getMail());
    }
}
