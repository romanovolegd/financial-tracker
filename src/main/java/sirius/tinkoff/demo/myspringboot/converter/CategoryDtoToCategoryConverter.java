package sirius.tinkoff.demo.myspringboot.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.dto.CategoryDto;


@Component
@RequiredArgsConstructor
public class CategoryDtoToCategoryConverter {
    public Category convert(CategoryDto dto) {
            return new Category()
                    .setName(dto.getName())
                    .setColour(dto.getColour())
                    .setIcon(dto.getIcon())
                    .setValue(dto.getValue());
    }


    public CategoryDto convert(Category category) {
        return new CategoryDto()
                .setCategoryId(category.getCategoryId())
                .setName(category.getName())
                .setColour(category.getColour())
                .setIcon(category.getIcon())
                .setValue(category.getValue());
    }
}
