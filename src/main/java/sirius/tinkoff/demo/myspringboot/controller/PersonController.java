package sirius.tinkoff.demo.myspringboot.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.demo.myspringboot.dto.PersonDto;
import sirius.tinkoff.demo.myspringboot.service.PersonService;

import java.util.List;

@RequestMapping("/person")
@RestController
@RequiredArgsConstructor
@Slf4j
public class PersonController {
    private final PersonService personService;

    @Operation(summary = "Метод для получения пользователя")
    @GetMapping(value = "/{personId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonDto getPerson(@PathVariable("personId") Long id) {
        return personService.getById(id);
    }

    @Operation(summary = "Метод для получения всех пользователей")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PersonDto> getAllPersons() {
        return personService.getAll();
    }

    @Operation(summary = "Метод для создания пользователя",
            description = "Создание пользователя по почте")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long createPerson(@RequestBody PersonDto personDto) {
        return personService.create(personDto);
    }

    @Operation(summary = "Метод для изменения данных пользователя по id")
    @PutMapping(value = "/{personId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updatePerson(@PathVariable("personId") Long id, @RequestBody PersonDto personDto) {
        personService.update(id, personDto);
    }

    @Operation(summary = "Метод для удаления данных пользователя по id")
    @DeleteMapping(value = "/{personId}")
    public void deletePerson(@PathVariable("personId") Long id) {
        personService.deleteById(id);
    }

}
