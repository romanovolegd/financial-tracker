package sirius.tinkoff.demo.myspringboot.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.demo.myspringboot.dto.WalletDto;
import sirius.tinkoff.demo.myspringboot.dto.WalletScreenDto;
import sirius.tinkoff.demo.myspringboot.service.WalletService;
import java.util.List;

@RequestMapping("/wallets")
@RestController
@RequiredArgsConstructor
@Slf4j
public class WalletController {

    private final WalletService walletService;

    @Operation(summary = "Метод для получения кошелька")
    @GetMapping(value = "/{walletId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletDto getWallet(@PathVariable("walletId") Long id) {
        return walletService.getById(id);
    }

    @Operation(summary = "Метод для получения всех кошельков по id пользователя")
    @GetMapping(value = "/person/{personId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WalletDto> getAllWallets(@PathVariable("personId") Long id) {return walletService.getAll(id);}

    @Operation(summary = "Метод для получения кошельков, общего баланса, курса валют")
    @GetMapping(value = "/person/{personId}/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletScreenDto getWalletScreen(@PathVariable("personId") Long id) {return walletService.getWalletScreen(id);}

    @Operation(summary = "Метод для создания кошелька",
            description = "Создание кошелька по названию, лимиту и валюте")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long createWallet(@RequestBody WalletDto walletDto) {
        return walletService.create(walletDto);
    }

    @Operation(summary = "Метод для изменения данных кошелька по id")
    @PutMapping(value = "/{walletId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateWallet(@PathVariable("walletId") Long id, @RequestBody WalletDto walletDto) {
        walletService.update(id, walletDto);
    }

    @Operation(summary = "Метод для удаления данных кошелька по id")
    @DeleteMapping(value = "/{walletId}")
    public void deleteWallet(@PathVariable("walletId") Long id) {
        walletService.deleteById(id);
    }
}
