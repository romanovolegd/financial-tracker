package sirius.tinkoff.demo.myspringboot.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.demo.myspringboot.dto.CurrencyDto;
import sirius.tinkoff.demo.myspringboot.service.CurrencyService;


@RequestMapping("/currencies")
@RestController
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;

    @Operation(summary = "Метод для получения валют")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public CurrencyDto getCurrency() {
        return currencyService.get();
    }
}
