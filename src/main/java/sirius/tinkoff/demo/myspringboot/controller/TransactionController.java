package sirius.tinkoff.demo.myspringboot.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.demo.myspringboot.dto.TransactionCategoryDto;
import sirius.tinkoff.demo.myspringboot.dto.TransactionDto;
import sirius.tinkoff.demo.myspringboot.service.TransactionService;

import java.util.List;

@RequestMapping("/transactions")
@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;

    @Operation(summary = "Метод для получения транзакции с категорией по id")
    @GetMapping(value = "/category/{transactionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionCategoryDto getTransactionCategory(@PathVariable("transactionId") Long id) {
        return transactionService.getByIdWithCategory(id);
    }

    @Operation(summary = "Метод для получения всех транзакций с категориями по id кошелька")
    @GetMapping(value = "/withCategory/{walletId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransactionCategoryDto> getAllTransactionsCategory(@PathVariable("walletId") Long id) {
        return transactionService.getAllTransactionsCategory(id);}

    @Operation(summary = "Метод для получения транзакции по id")
    @GetMapping(value = "/{transactionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionDto getTransaction(@PathVariable("transactionId") Long id) {
        return transactionService.getById(id);
    }

    @Operation(summary = "Метод для получения всех транзакций по id кошелька")
    @GetMapping(value = "/wallet/{walletId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransactionDto> getAllTransactions(@PathVariable("walletId") Long id) {return transactionService.getAllTransactions(id);}

    @Operation(summary = "Метод для создания транзакции",
            description = "Создание транзакции ")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createTransaction(@RequestBody TransactionDto transactionDto) {
        transactionService.create(transactionDto);
    }

    @Operation(summary = "Метод для изменения транзакции по id")
    @PutMapping(value = "/{transactionId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateTransaction(@PathVariable("transactionId") Long id, @RequestBody TransactionDto transactionDto) {
        transactionService.update(id, transactionDto);
    }

    @Operation(summary = "Метод для удаления транзакции по id")
    @DeleteMapping(value = "/{transactionId}")
    public void deleteTransaction(@PathVariable("transactionId") Long id) {
        transactionService.deleteById(id);
    }
}
