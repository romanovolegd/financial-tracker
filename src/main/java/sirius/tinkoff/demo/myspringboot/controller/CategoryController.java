package sirius.tinkoff.demo.myspringboot.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sirius.tinkoff.demo.myspringboot.dto.CategoryDto;
import sirius.tinkoff.demo.myspringboot.service.CategoryService;

import java.util.List;


@RequestMapping("/categories")
@RestController
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @Operation(summary = "Метод для получения категории по id")
    @GetMapping(value = "/{сategoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto getCategory(@PathVariable("сategoryId") Long id) {
        return categoryService.getById(id);
    }


    @Operation(summary = "Метод для получения всех категорий")
    @GetMapping(value = "/person/{personId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getAllCategories(@PathVariable("personId") Long id) {
        return categoryService.getAllCategories(id);
    }

    @Operation(summary = "Метод для получения всех категорий пользователя по доходу/расходу")
    @GetMapping(value = "/person/{personId}/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getCategories(@PathVariable("personId") Long id, @PathVariable("value") Integer value) {
        return categoryService.getCategories(id, value);
    }

    @Operation(summary = "Метод для создания категории",
            description = "Создание категории ")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createCategory(@RequestBody CategoryDto categoryDto) {
        categoryService.create(categoryDto);
    }

    @Operation(summary = "Метод для изменения данных категории по id")
    @PutMapping(value = "/{categoryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateCategory(@RequestBody CategoryDto categoryDto, @PathVariable("categoryId") Long id ) {
        categoryService.update(categoryDto, id);
    }

    @Operation(summary = "Метод для удаления данных категории по id")
    @DeleteMapping(value = "/{categoryId}")
    public void deleteCategory(@PathVariable("categoryId") Long id) {
        categoryService.deleteById(id);
    }
}
