package sirius.tinkoff.demo.myspringboot.service.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rates {
    @JsonProperty("RUB")
    BigDecimal rub;
    @JsonProperty("EUR")
    BigDecimal eur;
    @JsonProperty("CHF")
    BigDecimal chf;
}
