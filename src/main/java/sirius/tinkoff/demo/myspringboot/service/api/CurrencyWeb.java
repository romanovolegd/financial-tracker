package sirius.tinkoff.demo.myspringboot.service.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyWeb {
    Boolean success;
    BigDecimal timestamp;
    String base;
    Rates rates;
}
