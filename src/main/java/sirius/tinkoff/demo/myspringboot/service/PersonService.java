package sirius.tinkoff.demo.myspringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.demo.myspringboot.converter.PersonDtoToPersonConverter;
import sirius.tinkoff.demo.myspringboot.data.entity.Person;
import sirius.tinkoff.demo.myspringboot.data.entity.PersonToCategory;
import sirius.tinkoff.demo.myspringboot.data.repository.CategoryRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonToCategoryRepository;
import sirius.tinkoff.demo.myspringboot.dto.PersonDto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final PersonDtoToPersonConverter personDtoToPersonConverter;
    private final PersonToCategoryRepository personToCategoryRepository;
    private final CategoryRepository categoryRepository;

    public PersonDto getById(Long id) {
        return personDtoToPersonConverter.convert(personRepository.getById(id));
    }

    public List<PersonDto> getAll() {
        return personRepository.findAll().stream()
                .map(personDtoToPersonConverter::convert)
                .collect(Collectors.toList());
    }

    public Long create(PersonDto personDto) {
        Optional<Person> optionalPerson = personRepository.findByMail(personDto.getMail());
        if (optionalPerson.isPresent()) {
            return optionalPerson.get().getPersonId();
        }
        else {
            Person convert = personDtoToPersonConverter.convert(personDto);
            Person person = personRepository.save(convert);
            LongStream.range(1,12)
                    .mapToObj(i -> new PersonToCategory()
                            .setPerson(person)
                            .setCategory(categoryRepository.getById(i))
                    )
                    .forEach(personToCategoryRepository::save);
            return person.getPersonId();
        }
    }

    public void update(Long id, PersonDto personDto) {
        Optional<Person> personOptional = personRepository.findById(id);
        if (personOptional.isPresent()) {

            Person person = personOptional.get();
            Person convert = personDtoToPersonConverter.convert(personDto);

            person.setMail(convert.getMail());
            personRepository.save(person);
        }
    }

    public void deleteById(Long id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()) {
            personToCategoryRepository.getAllByPersonPersonId(id);
            personRepository.delete(person.get());
        }
    }
}
