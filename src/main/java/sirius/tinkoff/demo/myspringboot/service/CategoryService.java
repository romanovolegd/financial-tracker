package sirius.tinkoff.demo.myspringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.demo.myspringboot.converter.CategoryDtoToCategoryConverter;
import sirius.tinkoff.demo.myspringboot.data.entity.*;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.data.repository.CategoryRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonToCategoryRepository;
import sirius.tinkoff.demo.myspringboot.dto.CategoryDto;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryDtoToCategoryConverter categoryDtoToCategoryConverter;
    private final CategoryRepository categoryRepository;
    private final PersonRepository personRepository;
    private final PersonToCategoryRepository personToCategoryRepository;

    public void deleteById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        category.ifPresent(categoryRepository::delete);
    }

    public void create(CategoryDto categoryDto) {
        Category convert = categoryDtoToCategoryConverter.convert(categoryDto);
        Category category = categoryRepository.save(convert);
        Long personId = categoryDto.getPersonId();
        PersonToCategory personToCategory = new PersonToCategory();
        Optional<Person> optionalPerson = personRepository.findById(personId);
        if (optionalPerson.isPresent()) {
            personToCategory.setPerson(optionalPerson.get());
            personToCategory.setCategory(category);
            personToCategoryRepository.save(personToCategory);
        }
    }

    public  List<CategoryDto> getAllCategories(Long id) {
        return personToCategoryRepository.getAllByPersonPersonId(id).stream()
                .map(PersonToCategory::getCategory)
                .map(categoryDtoToCategoryConverter::convert)
                .collect(Collectors.toList());
    }

    public CategoryDto getById(Long id) {
        return categoryDtoToCategoryConverter.convert(categoryRepository.getById(id));
    }

    public void update(CategoryDto categoryDto, Long id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isPresent()) {

            Category category = categoryOptional.get();
            Category convert = categoryDtoToCategoryConverter.convert(categoryDto);

            category.setColour(convert.getColour());
            category.setName(convert.getName());
            category.setIcon(convert.getIcon());
            category.setValue(convert.getValue());

            categoryRepository.save(category);
        }
    }

    public List<CategoryDto> getCategories(Long id, Integer value) {
        return personToCategoryRepository.getAllByPersonPersonId(id).stream()
                .map(PersonToCategory::getCategory)
                .map(categoryDtoToCategoryConverter::convert)
                .filter(category -> category.getValue().equals(value))
                .collect(Collectors.toList());
    }
}
