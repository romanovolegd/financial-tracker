package sirius.tinkoff.demo.myspringboot.service.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "currency", url ="https://openexchangerates.org/api/latest.json?app_id=542786d199e54737af36316b9c0a30b1")
public interface CurrencyApiInterface {
    @RequestMapping(method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    CurrencyWeb getCurrency();
}
