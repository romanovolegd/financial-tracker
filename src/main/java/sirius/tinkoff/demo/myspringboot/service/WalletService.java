package sirius.tinkoff.demo.myspringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.demo.myspringboot.converter.WalletDtoToWalletConverter;
import sirius.tinkoff.demo.myspringboot.data.entity.Currency;
import sirius.tinkoff.demo.myspringboot.data.entity.Person;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;
import sirius.tinkoff.demo.myspringboot.data.repository.CurrencyRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.PersonRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.WalletRepository;
import sirius.tinkoff.demo.myspringboot.dto.WalletDto;
import sirius.tinkoff.demo.myspringboot.dto.WalletScreenDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WalletService {

    private final WalletRepository walletRepository;
    private final WalletDtoToWalletConverter walletDtoToWalletConverter;
    private final PersonRepository personRepository;
    private final CurrencyService currencyService;
    private final CurrencyRepository currencyRepository;
    private final TransactionService transactionService;


    public WalletDto getById(Long id) {
        Wallet wallet = walletRepository.getById(id);
        return countAll(wallet);
    }

    public List<WalletDto> getAll(Long id) {
        Person person = personRepository.getById(id);
        List<Wallet> walletList = person.getWallet();
        return walletList.stream()
                .map(this::countAll)
                .collect(Collectors.toList());
    }

    public WalletScreenDto getWalletScreen(Long personId) {
        List<WalletDto> walletDtoList = getAll(personId);
        WalletScreenDto walletScreenDto = new WalletScreenDto();
        List<BigDecimal> consumptionList = new ArrayList<>();
        List<BigDecimal> incomeList = new ArrayList<>();
        walletDtoList.forEach(walletDto -> {

            Currency oldCurrency = currencyRepository.getByTitle(walletDto.getCurrency());
            Currency newCurrency = currencyRepository.getByTitle("RUB");

            consumptionList.add(transactionService.convertValue(BigDecimal
                   .valueOf(Double.parseDouble(walletDto.getConsumption())), oldCurrency, newCurrency));
            incomeList.add(transactionService.convertValue(BigDecimal
                    .valueOf(Double.parseDouble(walletDto.getIncome())), oldCurrency, newCurrency));
        }
        );

        BigDecimal consumption = consumptionList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal income = incomeList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal balance = income.subtract(consumption);

        walletScreenDto.setWallets(walletDtoList);
        walletScreenDto.setConsumption(consumption.toString());
        walletScreenDto.setIncome(income.toString());
        walletScreenDto.setBalance(balance.toString());
        walletScreenDto.setCurrencyDto(currencyService.get());

        return walletScreenDto;
    }

    public Long create(WalletDto walletDto) {
        Wallet convert = walletDtoToWalletConverter.convert(walletDto);
        convert.setCurrency(currencyRepository.getByTitle(walletDto.getCurrency()));
        return walletRepository.save(convert).getWalletId();
    }

    public void update(Long id, WalletDto walletDto) {
        Optional<Wallet> walletOptional = walletRepository.findById(id);
        if (walletOptional.isPresent()) {

            Wallet wallet = walletOptional.get();
            Wallet convert = walletDtoToWalletConverter.convert(walletDto);
            wallet.setCurrency(convert.getCurrency());
            wallet.setName(convert.getName());
            wallet.setLimit(convert.getLimit());
            wallet.setPerson(convert.getPerson());
            walletRepository.save(wallet);
            transactionService.updateTransactionsIfNeeded(wallet);
        }

    }

    public void deleteById(Long id) {
        Optional<Wallet> wallet = walletRepository.findById(id);
        wallet.ifPresent(walletRepository::delete);
    }

    public BigDecimal countIncome(Wallet wallet) {
        return wallet.getTransaction().stream()
                .filter(transaction -> transaction.getCategory().getValue() == 0)
                .map(Transaction::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal countConsumption(Wallet wallet) {
        return wallet.getTransaction().stream()
                .filter(transaction -> transaction.getCategory().getValue() == 1)
                .map(Transaction::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private WalletDto countAll(Wallet wallet) {
        WalletDto walletDto = walletDtoToWalletConverter.convert(wallet);
        BigDecimal income = countIncome(wallet);
        BigDecimal consumption = countConsumption(wallet);
        BigDecimal amountMoney = income.subtract(consumption);
        walletDto.setIncome(income.toString());
        walletDto.setConsumption(consumption.toString());
        walletDto.setAmountMoney(amountMoney.toString());
        if (wallet.getLimit().equals(BigDecimal.valueOf(0))) {
            walletDto.setIsExceeded(false);
        } else {
            walletDto.setIsExceeded(consumption.compareTo(wallet.getLimit()) > 0);
        }
        return walletDto;
    }

}

