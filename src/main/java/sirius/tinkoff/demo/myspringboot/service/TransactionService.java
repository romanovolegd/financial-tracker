package sirius.tinkoff.demo.myspringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.demo.myspringboot.converter.TransactionCategoryDtoToTransactionCategoryConverter;
import sirius.tinkoff.demo.myspringboot.converter.TransactionDtoToTransactionConverter;
import sirius.tinkoff.demo.myspringboot.data.entity.Category;
import sirius.tinkoff.demo.myspringboot.data.entity.Currency;
import sirius.tinkoff.demo.myspringboot.data.entity.Transaction;
import sirius.tinkoff.demo.myspringboot.data.entity.Wallet;
import sirius.tinkoff.demo.myspringboot.data.repository.CategoryRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.TransactionRepository;
import sirius.tinkoff.demo.myspringboot.data.repository.WalletRepository;
import sirius.tinkoff.demo.myspringboot.dto.TransactionCategoryDto;
import sirius.tinkoff.demo.myspringboot.dto.TransactionDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final TransactionDtoToTransactionConverter transactionDtoToTransactionConverter;
    private final WalletRepository walletRepository;
    private final CategoryRepository categoryRepository;
    private final TransactionCategoryDtoToTransactionCategoryConverter transactionCategoryDtoToTransactionCategoryConverter;


    public TransactionDto getById(Long id) {
       return transactionDtoToTransactionConverter.convert(transactionRepository.getById(id));
    }

    public List<TransactionDto> getAllTransactions(Long id) {
        return walletRepository.getById(id).getTransaction().stream()
                .map(transactionDtoToTransactionConverter::convert)
                .collect(Collectors.toList());
    }

    public void create(TransactionDto transactionDto) {
        Transaction convert = transactionDtoToTransactionConverter.convert(transactionDto);
        Transaction properValueTransaction = convertCurrencyIfNeeded(convert);
        transactionRepository.save(properValueTransaction);
    }

    public void update(Long id, TransactionDto transactionDto) {
        Optional<Transaction> transactionOptional = transactionRepository.findById(id);
        if (transactionOptional.isPresent()) {
            Transaction convert = transactionDtoToTransactionConverter.convert(transactionDto);
            convert.setTransactionId(id);
            transactionRepository.save(convertCurrencyIfNeeded(convert));
        }
    }

    public void deleteById(Long id) {
        Optional<Transaction> transaction = transactionRepository.findById(id);
        transaction.ifPresent(transactionRepository::delete);
    }

    public TransactionCategoryDto getByIdWithCategory(Long id) {
        Transaction transaction = transactionRepository.getById(id);
        Category category = categoryRepository.getById(transaction.getCategory().getCategoryId());
        return transactionCategoryDtoToTransactionCategoryConverter.convert(transaction, category);
    }

    public List<TransactionCategoryDto> getAllTransactionsCategory(Long id) {
        Wallet wallet = walletRepository.getById(id);
        List<Transaction> transactionList = wallet.getTransaction();
        List<Category> categoryList = new ArrayList<>();
        transactionList.forEach(transaction -> categoryList
                .add(categoryRepository.getById(transaction.getCategory().getCategoryId())));
        List<TransactionCategoryDto> transactionCategoryDtoList = new ArrayList<>();
        IntStream.range(0, categoryList.size())
                .mapToObj(i -> transactionCategoryDtoToTransactionCategoryConverter
                        .convert(transactionList.get(i), categoryList.get(i)))
                .forEach(transactionCategoryDtoList::add);

        return transactionCategoryDtoList;
    }
    private Transaction convertCurrencyIfNeeded(Transaction transaction) {
        Currency walletCurrency = transaction.getWallet().getCurrency();
        Currency transactionCurrency = transaction.getCurrency();
        if (!walletCurrency.getTitle().equals(transactionCurrency.getTitle())) {
            BigDecimal oldValue = transaction.getValue();
            transaction.setValue(convertValue(oldValue, transactionCurrency, walletCurrency));
            transaction.setCurrency(walletCurrency);
        }
        return transaction;
    }

    public BigDecimal convertValue(BigDecimal value, Currency originalCurrency, Currency newCurrency) {
        BigDecimal baseCurrencyValue = value.multiply(originalCurrency.getCourse());
        return baseCurrencyValue.divide(newCurrency.getCourse(),2, RoundingMode.HALF_EVEN);
    }


    public void updateTransactionsIfNeeded(Wallet wallet) {
        List<Transaction> transactionList = wallet.getTransaction();
        if(!transactionList.isEmpty()) {
            Currency oldCurrency = transactionList.get(0).getCurrency();
            Currency newCurrency = wallet.getCurrency();
            if (!oldCurrency.getTitle().equals(newCurrency.getTitle())) {
                transactionList.forEach(transaction -> transaction
                        .setValue(convertValue(transaction.getValue(), oldCurrency,newCurrency))
                        .setCurrency(newCurrency));
                transactionRepository.saveAll(transactionList);
            }
        }
    }
}

