package sirius.tinkoff.demo.myspringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.tinkoff.demo.myspringboot.data.entity.Currency;
import sirius.tinkoff.demo.myspringboot.data.repository.CurrencyRepository;
import sirius.tinkoff.demo.myspringboot.dto.CurrencyDto;
import sirius.tinkoff.demo.myspringboot.service.api.CurrencyApiInterface;
import sirius.tinkoff.demo.myspringboot.service.api.CurrencyWeb;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@RequiredArgsConstructor
public class CurrencyService {
    private  final CurrencyRepository currencyRepository;
    private final CurrencyApiInterface currencyApiInterface;
    public CurrencyDto get() {
        return new CurrencyDto()
                .setFirstCurrency(currencyRepository.getById(2L).getTitle())
                .setFirstCourse(currencyRepository.getById(2L).getCourse().toString())
                .setFirstIsUp(currencyRepository.getById(2L).getIsUp())
                .setSecondCurrency(currencyRepository.getById(3L).getTitle())
                .setSecondCourse(currencyRepository.getById(3L).getCourse().toString())
                .setSecondIsUp(currencyRepository.getById(3L).getIsUp())
                .setThirdCurrency(currencyRepository.getById(4L).getTitle())
                .setThirdCourse(currencyRepository.getById(4L).getCourse().toString())
                .setThirdIsUp(currencyRepository.getById(4L).getIsUp());
    }

    public void update(Currency currency, BigDecimal newCourse){
        if (newCourse.compareTo(currency.getCourse()) > 0) {
            currency.setIsUp(true);
        }
        else if (newCourse.compareTo(currency.getCourse()) < 0) {
            currency.setIsUp(false);
        }
        currency.setCourse(newCourse);
        currencyRepository.save(currency);
    }

    public void getCurrencyAndUpdate() {
        CurrencyWeb currencyWeb = currencyApiInterface.getCurrency();

        Currency currencyUsd = currencyRepository.getByTitle("USD");
        BigDecimal newUsdCourse = currencyWeb
                .getRates()
                .getRub()
                .setScale(2, RoundingMode.HALF_UP);
        update(currencyUsd, newUsdCourse);

        Currency currencyEur = currencyRepository.getByTitle("EUR");
        BigDecimal newEurCourse = currencyUsd
                .getCourse()
                .divide(currencyWeb.getRates().getEur(), 2, RoundingMode.HALF_UP);
        update(currencyEur, newEurCourse);

        Currency currencyChf = currencyRepository.getByTitle("CHF");
        BigDecimal newChfCourse = currencyUsd
                .getCourse()
                .divide(currencyWeb.getRates().getChf(), 2, RoundingMode.HALF_UP);
        update(currencyChf, newChfCourse);
    }
}
