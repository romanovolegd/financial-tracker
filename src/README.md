

## Пример создания и публикации Docker образа

1) Сбор и запуск приложения + БД
```shell script
mvn clean package -Dmaven.test.skip=true
docker run -d -p 5432:5432 \
    --env POSTGRES_USER=root --env POSTGRES_PASSWORD=password \
    --name=pg_processor \
    postgres:13.1
java -jar \
    -DDB_NAME="root" -DDB_USERNAME="root" -DDB_PASSWORD="password" \
    target/trx-processor.jar
```
- inside container
```shell script
docker exec -it c72166020de6 bash
# install ps
apt-get update && apt-get install -y procps
```

2) Запаковка приложения в докер + публикация + docker-compose
- запаковка в докер
```shell script
docker build -t sirius-app .

docker tag sirius-app:latest alexyakovlev90/sirius-app:v1
docker push alexyakovlev90/sirius-app:v1

docker run -d -p 9090:9090 \
    --env DB_HOST=pg_processor --env DB_NAME=root --env DB_USERNAME=root --env DB_PASSWORD=password \
    --name=sirius-app \
    alexyakovlev90/sirius-app:latest
```
- Network
```shell script
docker network create test_network

docker run -d -p 5432:5432 \
    --env POSTGRES_USER=root --env POSTGRES_PASSWORD=password \
    --name=pg_processor --network test_network \
    postgres:13.1
docker run -d -p 9090:9090 \
    --env DB_HOST=pg_processor --env DB_NAME=root --env DB_USERNAME=root --env DB_PASSWORD=password \
    --name=sirius-app --network test_network \
    alexyakovlev90/sirius-app:v1
```


#### Полезные команды
```shell script
docker images
docker ps
docker kill
docker rm
docker network rm <name>

docker logs -f <u_container_id>
docker exec -it <u_container_id> bash

docker rm $(docker ps -a -q) # удалит все незапущенные контейнеры
docker rmi $(docker images -q) # удаляет image, если от него не зависят запущенные контейнеры

docker-compose up -d
docker-compose down
```

